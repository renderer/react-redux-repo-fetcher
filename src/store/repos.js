import { createSlice } from 'redux-starter-kit';

const reposSlice = createSlice({
  slice: 'repos',
  initialState: {
    loading: false,
    username: null,
    repos: []
  },
  reducers: {
    fetchRepos: (state, action) => ({
      ...state,
      username: action.payload,
      loading: true
    }),
    fetchReposSuccess: (state, action) => ({
      ...state,
      loading: false,
      repos: action.payload
    })
  }
});

export const loadRepos = username => {
  return async dispatch => {
    dispatch(reposSlice.actions.fetchRepos(username));
    const repos = await fetch(
      `https://api.github.com/users/${username}/repos`
    ).then(res => res.json());
    dispatch(reposSlice.actions.fetchReposSuccess(repos));
  };
};

export default reposSlice;
