import { configureStore } from 'redux-starter-kit';
import { combineReducers } from 'redux';

import reposSlice from './repos';

const reducer = combineReducers({
  repos: reposSlice.reducer
});

export default () => {
  const store = configureStore({ reducer });
  return store;
};
