import React from 'react';

import createStore from './store';

import RepoUsernameSearchInput from './containers/RepoUsernameSearchInput';
import RepoList from './containers/RepoList';

import { Provider } from 'react-redux';

const store = createStore();

const App = () => (
  <Provider store={store}>
    <RepoUsernameSearchInput />
    <RepoList />
  </Provider>
);

export default App;
