import React, { useState } from 'react';

export default ({ onFetchRepo }) => {
  const [username, setUsername] = useState('');

  const handleKeyup = keyCode => {
    if (keyCode === 13) {
      onFetchRepo && onFetchRepo(username);
    }
  };

  return (
    <input
      type="text"
      value={username}
      onChange={e => setUsername(e.target.value)}
      onKeyUp={e => handleKeyup(e.keyCode)}
    />
  );
};
