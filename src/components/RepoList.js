import React from 'react';

export default ({ repos, loading, username }) => {
  if (loading) {
    return <div>Loading repos for {username}</div>;
  }
  return (
    <ul>
      {repos.map(repo => (
        <li key={repo.id}>{repo.name}</li>
      ))}
    </ul>
  );
};
