import RepoList from '../components/RepoList';

import { connect } from 'react-redux';

export default connect(state => ({
  loading: state.repos.loading,
  repos: state.repos.repos,
  username: state.repos.username
}))(RepoList);
