import RepoUsernameSearchInput from '../components/RepoUsernameSearchInput';

import { loadRepos } from '../store/repos';
import { connect } from 'react-redux';

export default connect(
  null,
  dispatch => ({
    onFetchRepo: username => dispatch(loadRepos(username))
  })
)(RepoUsernameSearchInput);
